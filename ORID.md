# Daily Report (2023/07/19)
# O (Objective): 
## 1. What did we learn today?
### We learned integration test, Spring Boot 3 layers and Controller Advice today.

## 2. What activities did you do? 
### I conducted a code review with my team members and carried out spring boot, unit test and Integration testing programming training.

## 3. What scenes have impressed you?
### I have been impressed by the integration testing today.
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Difficult.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is showing the code review of Pair programming yesterday with the team members. Because this made me see the results of two people's efforts. I can learn from others' architectural styles and business ideas, and also improve my understanding of my own technology and business. It also helps to cultivate a technical atmosphere within the team.


# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use Java stream instead of loop and try to reduce the use of if else statements as much as possible when I write the functions, and I will use controller advice to achieve global exception control.
### I will develop a good coding habit of committing step by step. And I will strengthen communication with group members when doing group assignments in order to set aside time to rehearse the content of the speech in advance and present better performance results.



