package com.afs.restapi.model;

public class Employee {
    public static final int AGE_LOW_LIMIT = 18;
    public static final int AGE_HIGH_LIMIT = 65;
    public static final int AGE_LIMIT_MATCH_SALARY = 30;
    public static final int SALARY_LIMIT_MATCH_AGE = 20000;
    private int id;
    private String name;
    private int age;
    private String gender;
    private int salary;

    private Boolean status;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Employee() {
    }

    public Employee(int id, String name, int age, String gender, int salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void merge(Employee employee) {
        this.salary = employee.salary;
    }

    public boolean ageNotMatch() {
        return age < AGE_LOW_LIMIT || age > AGE_HIGH_LIMIT;
    }

    public boolean ageNotMatchSalary() {
        return age > AGE_LIMIT_MATCH_SALARY && salary < SALARY_LIMIT_MATCH_AGE;
    }
}
