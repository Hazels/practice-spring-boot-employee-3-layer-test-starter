package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insert(Employee employee) {
        if (employee.ageNotMatch()) {
            throw new AgeErrorException();
        } else if (employee.ageNotMatchSalary()) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);

    }


    public Employee update(int id, Employee employee) {
        if (employee.getStatus()) {
            return employeeRepository.update(id, employee);
        }
        return null;
    }

    public void delete(int id) {
        employeeRepository.findById(id).setStatus(false);
    }

}
