package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class EmployeeServiceTest {

    private EmployeeRepository repository = mock(EmployeeRepository.class);
    private EmployeeService employeeService = new EmployeeService(repository);

    @Test
    void should_throw_exception_and_not_call_repo_when_insert_employee_given_an_employee_age15_or_age66() {
        Employee employeeYoung = new Employee(1, "ZhangSan", 15, "Male", 1500);
        Employee employeeOld = new Employee(2, "Lisi", 66, "Male", 20000);

        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insert(employeeYoung));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insert(employeeOld));

        Mockito.verify(repository, times(0)).insert(any());
    }

    @Test
    void should_throw_exception_and_not_call_repo_when_insert_employee_given_employees_age_over_30_with_a_salary_below_20000() {
        Employee employeeOld = new Employee(2, "Lisi", 65, "Male", 19000);

        Assertions.assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.insert(employeeOld));

        Mockito.verify(repository, times(0)).insert(any());
    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_employees() {
        Employee employee = new Employee(3, "Jaden", 25, "Male", 19000);
        Employee employeeToReturn = new Employee(3, "Jaden", 25, "Male", 19000);
        employeeToReturn.setStatus(true);

        when(repository.insert(any())).thenReturn(employeeToReturn);

        Employee employeeSaved = employeeService.insert(employee);

        assertTrue(employeeSaved.getStatus());
        verify(repository).insert(argThat(
                e -> {
                    assertTrue(e.getStatus());
                    return true;
                }
        ));
    }

    @Test
    public void should_set_employee_status_false_when_delete_employee_given_id() {
        Employee employee = new Employee(3, "Jaden", 25, "Male", 19000);
        employee.setStatus(true);
        when(repository.findById(1)).thenReturn(employee);

        employeeService.delete(1);

        assertEquals(false, employee.getStatus());
    }


    @Test
    public void should_update_fail_when_update_employee_given_employee_with_status_false(){
        Employee employee = new Employee(3, "Jaden", 25, "Male", 19000);
        employee.setStatus(false);

        employeeService.update(3, employee);
        verify(repository, times(0)).update(3, employee);
    }

}